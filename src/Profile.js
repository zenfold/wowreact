/**
 * Created by david on 2017-03-11.
 */
import React, {Component} from 'react';
import './App.css';

class Profile extends Component {

    render() {
        let artist = {
            name: '',
            followers: {total: ''},
            images: [{url: ''}],
            genres: []
        };

        if (this.props.artist !== null) {
            artist = this.props.artist;
        }
        return (
            <div className="profile">
                <img className="img" alt="Profile" src={artist.images[0].url}></img>
                <div className="info">
                    <div className="name">{artist.name}</div>
                    <div className="followers">{artist.followers.total} Followers</div>

                    <div className="genres">
                        {
                            artist.genres.map((genre, k) => {
                                genre = genre !== artist.genres[artist.genres.length - 1] ? `${genre}, ` : `${genre}`;
                                return (
                                    <span key={k}>{genre}</span>
                                )
                            })
                        }
                    </div>
                </div>
            </div>

        );
    }

}

export default Profile;
