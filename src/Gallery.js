/**
 * Created by david on 2017-03-12.
 */
import React, {Component} from 'react';
import './App.css';
//import {FormGroup, FormControl, InputGroup, Glyphicon} from 'react-bootstrap';

class Gallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            playingUrl: '',
            audio: null,
            playing: false
        };
    }
    playAudio(previewUrl) {
        let audio = new Audio(previewUrl);
        if(this.state.playing) {
            if(this.state.playingUrl === previewUrl) {
                this.state.audio.pause();
                this.setState({
                    playing:false
                });
            } else {
                this.state.audio.pause();
                audio.play();
                this.setState({
                    playing:true,
                    playingUrl: previewUrl,
                    audio
                });
            }

        } else {
            audio.play();
            this.setState({
                playing: true,
                playingUrl: previewUrl,
                audio
            });
        }
    }
    render() {
        const {tracks} = this.props;
        return (
            <div className="gallery">
                {tracks.map((track, k) => {
                    const trackImg = track.album.images[0].url;
                    return (
                        <div key={k} className="track" onClick={()=>this.playAudio(track.preview_url)}>
                            <img src={trackImg} className="img" alt="track"/>
                            <div className="play">
                                <div className="inner">
                                    {
                                        this.state.playingUrl === track.preview_url ? <span>| |</span> : <span>&#9654;</span>
                                    }
                                </div>
                            </div>
                            <p className="text">
                                {track.name}
                            </p>
                        </div>
                    );
                })}
            </div>
        );
    }
}

export default Gallery;
